package com.chitrar.myapplication.ui.main.network

import com.chitrar.myapplication.ui.main.model.Products
import retrofit2.Call
import retrofit2.http.GET

interface ProductAPIs {

    //https://dummyjson.com/products
    @GET("products")
    fun getProducts(): Call<Products>
}