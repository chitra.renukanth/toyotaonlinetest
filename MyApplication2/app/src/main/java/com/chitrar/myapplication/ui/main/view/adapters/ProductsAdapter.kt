package com.chitrar.myapplication.ui.main.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.chitrar.myapplication.databinding.ItemProductDetailBinding
import com.chitrar.myapplication.ui.main.model.Product

class ProductsAdapter :
    ListAdapter<Product, ProductsAdapter.ProductViewHolder>(DiffCallback) {

    class ProductViewHolder(private val itemViewBinding: ItemProductDetailBinding) :
        ViewHolder(itemViewBinding.root) {
        fun bindData(product: Product) {
            itemViewBinding.product = product
            itemViewBinding.executePendingBindings()
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Product>() {
        override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem.getImageUrl() == newItem.getImageUrl()
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(
            ItemProductDetailBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bindData(product = getItem(position))
    }

}