package com.chitrar.myapplication.ui.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chitrar.myapplication.ui.main.model.Product
import com.chitrar.myapplication.ui.main.model.Products
import com.chitrar.myapplication.ui.main.network.ProductsRepository
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : ViewModel() {
    private val repository: ProductsRepository
    var onProductSuccess: MutableLiveData<Products?> = MutableLiveData()
    var onProductFail: MutableLiveData<Boolean> = MutableLiveData()
    var progressBarVisibility: MutableLiveData<Boolean> = MutableLiveData()
    private var productsListData = MutableLiveData<List<Product>>()
    val productsList: LiveData<List<Product>> = productsListData

    init {
        repository = ProductsRepository()
    }

    fun getProducts() {
        progressBarVisibility.value = true
        viewModelScope.launch {
            repository.getProducts(object : Callback<Products> {
                override fun onResponse(call: Call<Products>, response: Response<Products>) {
                    progressBarVisibility.value = false
                    if (response.isSuccessful && response.body()?.products != null) {
                        onProductSuccess.value = response.body()
                        onProductSuccess.value?.products?.let {
                            productsListData.value =  it
                        }
                    } else {
                        onProductFail.value = true
                    }
                }

                override fun onFailure(call: Call<Products>, t: Throwable) {
                    progressBarVisibility.value = false
                    call.cancel()
                    onProductFail.value = true
                }

            })
        }

    }
}