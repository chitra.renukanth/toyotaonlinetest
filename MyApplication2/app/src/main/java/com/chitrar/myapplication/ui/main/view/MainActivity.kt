package com.chitrar.myapplication.ui.main.view

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.chitrar.myapplication.R
import com.chitrar.myapplication.databinding.ActivityMainBinding
import com.chitrar.myapplication.ui.main.viewmodel.MainViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
        initUi()
    }

    private fun initUi() {
        setObservers()
        updateProgressBar()
        viewModel.getProducts()
        initHyperLinkText()
    }

    private fun initHyperLinkText() {
        var textValue = SpannableString(getString(R.string.error_message))
        var hyperLinkText = getString(R.string.try_again)
        var startIndex = textValue.lastIndexOf(hyperLinkText)
        textValue.setSpan(
            object : ClickableSpan() {
                override fun onClick(p0: View) {
                    viewModel.getProducts()
                }

                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.isUnderlineText = true
                }
            },
            startIndex,
            startIndex + hyperLinkText.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        binding.errorMessage.text = textValue
        binding.errorMessage.movementMethod = LinkMovementMethod.getInstance()
        binding.errorMessage.highlightColor = Color.TRANSPARENT
    }

    private fun setObservers() {
        viewModel.onProductSuccess.observe(this, Observer { products ->
            products?.products?.let { productList ->
                if (productList.isNotEmpty()) {
                    updateFragment()
                } else {
                    displayEmptyListMessage()
                }
            } ?: run {
                displayEmptyListMessage()
            }
        })

        viewModel.onProductFail.observe(this, Observer { isFailed ->
            binding.errorMessage.isVisible = isFailed
        })
    }

    private fun updateFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, MainFragment())
            .commitNow()

    }

    private fun updateProgressBar() {
        viewModel.progressBarVisibility.observe(this, Observer { isVisible ->
            binding.circularProgressBar.isVisible = isVisible
        })
    }

    private fun displayEmptyListMessage() {
        binding.errorMessage.isVisible = true
        binding.errorMessage.text = getString(R.string.empty_message)
    }
}