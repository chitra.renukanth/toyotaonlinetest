package com.chitrar.myapplication.ui.main.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Products( val products: List<Product>?,
                     val total: Int,
                     val skip: Int,
                     val limit: Int
) : Parcelable

@Parcelize
data class Product( val id: Int,
                    val title: String?,
                     val description: String?,
                     val images: List<String>?
) : Parcelable {
    fun getImageUrl(): String {
        return if(images != null && images.isNotEmpty()) {
            images[0]
        } else {
            ""
        }
    }
}

//title, description and images