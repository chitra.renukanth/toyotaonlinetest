package com.chitrar.myapplication.ui.main.view

import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import com.chitrar.myapplication.databinding.FragmentMainBinding
import com.chitrar.myapplication.ui.main.view.adapters.ProductsAdapter
import com.chitrar.myapplication.ui.main.viewmodel.MainViewModel


class MainFragment : Fragment() {

    private lateinit var viewModel: MainViewModel

    private val binding: FragmentMainBinding by lazy {
        FragmentMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity())[MainViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initUI()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI() {
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.recyclerView.adapter = ProductsAdapter()
        binding.recyclerView.addItemDecoration(
            DividerItemDecoration(requireContext(), 1)
        )
    }

}