package com.chitrar.myapplication.ui.main.network

import com.chitrar.myapplication.ui.main.model.Products
import retrofit2.Callback

class ProductsRepository {

    var productApi: ProductAPIs = ProductApiClient.getClient().create(ProductAPIs::class.java)

    fun getProducts(productResponse: Callback<Products>) {
        var productsRequest = productApi.getProducts()
        productsRequest.enqueue(productResponse)
    }


}